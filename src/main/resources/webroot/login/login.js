addEventListener("load", function() {
	console.log("LOGIN LOADED");
	document.getElementById("submitButton").addEventListener("click", function(e) {
		document.getElementById("login_alert_id").style.display = "none";
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;
		console.log("LOGIN LOADED username: "+username);
		var redirect_url = window.localStorage.redirectURL;
		console.log("LOGIN LOADED currentttt window.localStorage.redirectURL : "+redirect_url);
		console.log("LOGIN LOADED window.localStorage.redirectURL : "+redirect_url);
		if(typeof(redirect_url) == 'undefined') {
			redirect_url = "/questions/";
		}
//		if(window.localStorage.redirectURL )
		var login_details_json = {
			"username" : username,
			"password" : password,
//			"return_url" : "/questions/"
			"return_url" : redirect_url
		};
		var xhr = new XMLHttpRequest();
		xhr.open("post", "/api/v1/users/login", true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				console.log("xhr.status: "+xhr.status);
				if(xhr.status == 200) {
					window.localStorage.redirectURL = undefined;
					var URL = xhr.getResponseHeader("location");
					var token = xhr.getResponseHeader("Authorization");
					window.localStorage.accessToken = token;
					window.localStorage.userId = username;
					window.location.href = URL;
				} else {
					console.log(" xhr responseText: "+xhr.responseText);
					document.getElementById("login_alert_id").style.display = "block";
//					document.getElementById("login_alert_info_id").innerHTML = "Wrong username or password";
				}
//				loadQuestionsPage(xhr);
//				console.log(xhr);
			} 
		};
		console.log("LOGIN LOADED login_details_json: "+JSON.stringify(login_details_json));
		xhr.send(JSON.stringify(login_details_json));
	})
	
	function loadQuestionsPage(xhr) {
		console.log("loadQuestionsPage");
		var URL = xhr.getResponseHeader("location");
//		window.location = URL;
//		console.log(xhr.getAllResponseHeaders());
		var xhrLoadQuestion = new XMLHttpRequest();
		console.log("inside login.js for loadQuestionsPage URL: "+URL);
		xhrLoadQuestion.open("get", URL, true);
		xhrLoadQuestion.setRequestHeader("Content-Type", "text/html");
		xhrLoadQuestion.setRequestHeader("Authorization", xhr.getResponseHeader("Authorization"));
		xhrLoadQuestion.onreadystatechange = function() {
			if (xhrLoadQuestion.readyState == 4 && xhrLoadQuestion.status == 200) {
				console.log(" Question page Status is 200");
//				console.log(xhr);
			} 
		};
		xhrLoadQuestion.send();
	}
});