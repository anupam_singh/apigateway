addEventListener("load", function() {
	console.log("QUESTION PAGE LOADED");
	console.log(" QUESTION PAGE window.localStorage.accessToken: "+window.localStorage.accessToken);
	console.log(" QUESTION PAGE TARGET: "+window.localStorage.targetpage);
	if(window.localStorage.targetpage == "SEARCHED_QUESTIONS") {
		getSearchedQuestions();
	} else if(window.localStorage.targetpage == "ALL_QUESTIONS") {
		getAllQuestions();
	} else {
		getAllQuestions();
	}
	
	function getAllQuestions() {
		console.log("getAllQuestions");
		document.getElementById("ques_H2_id").innerHTML = "All Questions";
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/v1/questions/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				var all_ques_div_id = document.getElementById("all_ques_div_id");
				if(xhr.status == 200 || xhr.status == 401) {
//					console.log(" ALL Questions responseText: "+xhr.responseText);
					var questionObj = JSON.parse(xhr.responseText);
					all_ques_div_id.style.display = "block";
					if(questionObj.length == 0) {
						all_ques_div_id.innerHTML = "Question is yet to be asked";
					} else {
						for (var i = 0; i < questionObj.length; i++) {
//							console.log(" getNewestQuestions title: "+questionObj[i].title);
							
							//DIV for one question element
							var main_DIV = document.createElement("div");
							all_ques_div_id.appendChild(main_DIV);
							
							createTitleDiv(questionObj[i],main_DIV);
							
							var myhr = document.createElement('hr');
							main_DIV.appendChild(myhr);
						}
					}
				} else if(xhr.status == 500 || xhr.status == 404) {
					all_ques_div_id.style.display = "block";
					all_ques_div_id.innerHTML = "Question is yet to be asked";
				}
			} 
		};
		xhr.send();
	}
	
	function getSearchedQuestions() {
		console.log("getSearchedQuestions");
		document.getElementById("ques_H2_id").innerHTML = "Search Result";
		console.log(" window.location.href: "+window.location.href);
		console.log(" window.location.search: "+window.location.search);
		var url = new URL(window.location.href);
		var query_str = url.searchParams.get("search_query");
		console.log("search_query: "+query_str);
		if(query_str == "" || query_str == "undefined") {
			all_ques_div_id.style.display = "block";
			all_ques_div_id.innerHTML = "No search input";
			return;
		}
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/v1/questions/results/?search_query=" + query_str, true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				var all_ques_div_id = document.getElementById("all_ques_div_id");
				if(xhr.status == 200 || xhr.status == 401) {
//					console.log(" ALL Questions responseText: "+xhr.responseText);
					var questionObj = JSON.parse(xhr.responseText);
					all_ques_div_id.style.display = "block";
					if(questionObj.length == 0) {
						all_ques_div_id.innerHTML = "No match found";
					} else {
						for (var i = 0; i < questionObj.length; i++) {
//							console.log(" getNewestQuestions title: "+questionObj[i].title);
							
							//DIV for one question element
							var main_DIV = document.createElement("div");
							all_ques_div_id.appendChild(main_DIV);
							
							createTitleDiv(questionObj[i],main_DIV);
							
							var myhr = document.createElement('hr');
							main_DIV.appendChild(myhr);
						}
					}
				} else if(xhr.status == 500 || xhr.status == 404) {
					all_ques_div_id.style.display = "block";
					all_ques_div_id.innerHTML = "No match found";
				}
			} 
		};
		xhr.send();
	}
	
	function createTitleDiv(questionObjElem,main_DIV) {
		//DIV for question title
		var title_div = document.createElement("div");
		title_div.id = questionObjElem._id;
		title_div.innerHTML = questionObjElem.title;
		
		title_div.addEventListener("click", function(e) {
//			console.log("QUESTION CLICKED e.target.id: "+e.target.id);
			window.location.href = "/questions/" + e.target.id;
			window.localStorage.questionId = e.target.id;
		})
		main_DIV.appendChild(title_div);
		
		title_div.onmouseover = function(e) {
//			console.log("title_div onmouseover e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "blue";
				document.getElementById(e.target.id).style.cursor = "pointer";
			}
		}
		title_div.onmouseout = function(e) {
//		console.log("title_div onmouseout e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "black";
			}
		}
		
		//DIV for posted time
		var div_time_date = document.createElement("div");
//		console.log("date from DB: "+questionObjElem.asked);
		div_time_date.innerHTML = questionObjElem.asked + "  ";
		div_time_date.style.textAlign = "right";
		main_DIV.appendChild(div_time_date);
		
		createUserDiv(questionObjElem,main_DIV);
		
	}
	
	function createUserDiv(questionObjElem,main_DIV) {
		//DIV for user who posted the question
		var div_user = document.createElement("div");
		div_user.id = questionObjElem.user.username + ":" + questionObjElem._id;
		div_user.innerHTML = questionObjElem.user.name;
		div_user.style.color = "green";
		div_user.style.textAlign = "right";
		main_DIV.appendChild(div_user);
		
		div_user.addEventListener("click", function(e) {
			console.log("USER CLICKED e.target.id: "+e.target.id);
			var res = e.target.id.split(":");
			window.location.href = "/users/profile/"+res[0];
		})
		
		div_user.onmouseover = function(e) {
//			console.log("div_useronmouseover e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "blue";
				document.getElementById(e.target.id).style.cursor = "pointer";
			}
		}
		div_user.onmouseout = function(e) {
//		console.log("div_user onmouseout e.target.id: "+e.target.id);
			if(document.getElementById(e.target.id) != null) {
				document.getElementById(e.target.id).style.color = "green";
			}
		}
	}
});