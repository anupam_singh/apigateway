addEventListener("load", function() {
	console.log(" QUESTION PAGE LOADED");
	console.log(" QUESTION PAGE window.localStorage.accessToken: "+window.localStorage.accessToken);
	
	var questions_xhr = new XMLHttpRequest();
	questions_xhr.open("get", "/api/questions/", true);
	questions_xhr.setRequestHeader("Content-Type", "text/html");
	questions_xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
	questions_xhr.onreadystatechange = function() {
		if (questions_xhr.readyState == 4) {
			if(questions_xhr.status == 200) {
				console.log(" questions responseText: "+questions_xhr.responseText);
				document.getElementById("question_id").innerHTML = questions_xhr.responseText;
				document.getElementById("all_questions").style.display = "block";
				document.getElementById("logoutButton").style.display = "block";
//				document.getElementById("logoutButton").addEventListener("click", function(e) {
//					window.localStorage.clear();
//					logout();
//				});
			} 
//			else if(xhr.status == 401) {
//				window.localStorage.clear();
//				window.location.href = "/login/";
//			}
		} 
	};
	questions_xhr.send();
	
	var xhr = new XMLHttpRequest();
	xhr.open("get", "/api/users/", true);
	xhr.setRequestHeader("Content-Type", "text/html");
	xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			if(xhr.status == 200) {
				console.log(" users responseText: "+xhr.responseText);
				var userObj = JSON.parse(xhr.responseText);
				document.getElementById("user_info_id").innerHTML = userObj.firstname;
				document.getElementById("user_info").style.display = "block";
				document.getElementById("logoutButton").style.display = "block";
				document.getElementById("homeButton").style.display = "block";
				document.getElementById("logoutButton").addEventListener("click", function(e) {
					window.localStorage.clear();
					logout();
				});
				document.getElementById("homeButton").addEventListener("click", function(e) {
					window.location.href = "/";
				});
			} else if(xhr.status == 401) {
				window.localStorage.clear();
				window.location.href = "/";
			}
		} 
	};
	xhr.send();
	
	function logout() {
		var xhr = new XMLHttpRequest();
		xhr.open("get", "/api/users/", true);
		xhr.setRequestHeader("Content-Type", "text/html");
		xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 401)) {
				window.location.href = "/";
			} 
		};
		xhr.send();
	}
});