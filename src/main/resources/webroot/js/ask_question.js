addEventListener("load", function() {
	console.log("ASK QUESTION LOADED");
	
	var token = window.localStorage.accessToken;
	if (typeof(token) !== 'undefined') {
		document.getElementById("post_ques_button_id").addEventListener("click", function(e) {
			var title = document.getElementById("question_title_id").value;
			var description = document.getElementById("question_desc_id").value;
			console.log("POST QUES CLICKED title: "+title);
			console.log("POST QUES CLICKED description: "+description);
			var post_ques_details_json = {
				"title" : title,
				"description" : description
//				"answers":[]
			};
			var xhr = new XMLHttpRequest();
			xhr.open("post", "/api/v1/questions/", true);
			xhr.setRequestHeader("Content-Type", "application/json");
			console.log("ASK QUESTION LOADED window.localStorage.accessToken: "+window.localStorage.accessToken);
			xhr.setRequestHeader("Authorization", window.localStorage.accessToken);
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					console.log("ASK QUESTION LOADED xhr.status: "+xhr.status);
					if(xhr.status == 200) {
						console.log(" Status is 200");
//						var URL = xhr.getResponseHeader("location");
//						var token = xhr.getResponseHeader("Authorization");
//						window.localStorage.accessToken = token;
//						window.location.href = URL;
//						window.location.href = "/";
//						loadQuestionsPage(xhr);
						console.log(" post questions responseText: "+xhr.responseText);
						window.location.href = "/questions/";
					} else if(xhr.status == 401 || xhr.status == 502) {
						window.localStorage.clear();
						window.localStorage.redirectURL = "/questions/ask/";
						console.log("Inside ASK QUES window.localStorage.redirectURL: "+window.localStorage.redirectURL)
						window.location.href = "/login/";
					}
				} 
			};
			console.log("POST QUES CLICKED post_ques_details_json: "+JSON.stringify(post_ques_details_json));
			xhr.send(JSON.stringify(post_ques_details_json));
		})
	} else {
		window.localStorage.clear();
		window.localStorage.redirectURL = "/questions/ask/";
		window.location.href = "/login/";
	}
	
});